import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import kotlin.system.exitProcess

internal class ExtendedHashMapTest {

    @Test
    fun ilocTest() {
        val map: ExtendedHashMap = ExtendedHashMap()
        map["value1"] = 1
        map["value2"] = 2
        map["value3"] = 3
        map["1"] = 10
        map["2"] = 20
        map["3"] = 30

        assertEquals(10, map.iloc[0])
        assertEquals(20, map.iloc[1])
        assertEquals(30, map.iloc[2])
        assertEquals(1, map.iloc[3])
        assertEquals(2, map.iloc[4])
        assertEquals(3, map.iloc[5])


        assertNull(map.iloc[6])
        assertNull(map.iloc[-1])
    }

    @Test
    fun plocTest(){
        val map = ExtendedHashMap()
        val emptyMap = ExtendedHashMap()
        map["2"] = 2
        map["3"] = 3
        map["(1, 5)"] = 100
        map["(5, 5)"] = 200
        map["(10, 5)"] = 300
        map["(1, 5, 3)"] = 400
        map["(5, 5, 4)"] = 500
        map["(10, 5, 5)"] = 600

        assertEquals(emptyMap, map.ploc[""])
        assertEquals(emptyMap, map.ploc["=a, <=10"])
        assertEquals(emptyMap, map.ploc["<1"])
        assertEquals(emptyMap, map.ploc["<-1"])
        assertEquals(emptyMap, map.ploc["><-1"])


        var correctMap = ExtendedHashMap()
        correctMap["2"] = 2
        assertEquals(correctMap, map.ploc["=2"]) // {2=2}
        correctMap["3"] = 3
        assertEquals(correctMap, map.ploc[">=2"]) // {2=2, 3=3}

        map["-10"] = -10
        assertEquals(correctMap, map.ploc[">-10"]) // {2=2, 3=3}

        correctMap["-10"] = -10
        assertEquals(correctMap, map.ploc[">=-10"]) // {-10=-10, 2=2, 3=3}



        correctMap = ExtendedHashMap()

        correctMap["(1, 5, 3)"] = 400
        correctMap["(5, 5, 4)"] = 500
        assertEquals(correctMap, map.ploc["<= 10, =5, <>5"]) // {(5, 5, 4)=500, (1, 5, 3)=400}

        correctMap["(10, 5, 5)"] = 600

        assertEquals(correctMap, map.ploc["<= 10, =5, >0"]) // {(5, 5, 4)=500, (1, 5, 3)=400, (10, 5, 5)=600}

    }

}